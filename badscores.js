// This code produces a runtime error.
// See if you can figure out why it's happening
// Bonus if you figure out how to fix the code to make it print the right result.
const teamScores = {
    "Hawks": 100,
    "Eagles": 95,
    "Falcons": 102
};

const team = "Hawks";
const score = teamScores[team];

score.push(101);

console.log("The score of the Hawks is " + score);
