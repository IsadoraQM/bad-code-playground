// This code has a logic error. 
// It doesn't produce any error messages, 
// but it's still incorrect, see if you can figure it out
const student = {
    name: "John",
    grade: 85,
    passed: false
};

if (student.grade > 60) {
    student.passed = false;
}

console.log(student.name + " passed: " + student.passed); // Expected: John passed: true
